﻿using UnityEngine;
using System.Collections;

public class FormCollectScript : MonoBehaviour {

	private Sprite thisSprite;

	public AudioSource powerupSound;

	void Start(){
		thisSprite = GetComponent<SpriteRenderer> ().sprite;
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			Collect ();
		}
	}

	private void Collect(){
		//Collect new form

		if (!powerupSound.isPlaying) {
			powerupSound.Play ();
		}

		GetComponent<SpriteRenderer> ().enabled = false;
		GetComponent<CircleCollider2D> ().enabled = false;

		SomeArgs theArgs = new SomeArgs {
			SomeParameterThatMightBeAClass = thisSprite
		};

		GameLib.EventManager.RaiseEvent("NewFormCollect", gameObject, theArgs);

		Destroy (this.gameObject, 0.5f);
	}
}
