﻿using UnityEngine;
using System.Collections;

public class StarScript : MonoBehaviour {

	public AudioSource starCollect;


	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			Collect ();
		}
	}

	private void Collect(){
		//Collect point

		if (!starCollect.isPlaying) {
			starCollect.Play ();	
		}

		GetComponent<SpriteRenderer> ().enabled = false;
		GetComponent<CircleCollider2D> ().enabled = false;

		SomeArgs theArgs = new SomeArgs {};

		GameLib.EventManager.RaiseEvent("StarCollect", gameObject, theArgs);

		Destroy (this.gameObject, 0.5f);
	}
}
