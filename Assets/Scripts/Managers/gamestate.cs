﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class gamestate : MonoBehaviour {

	// Declare properties
	private static gamestate instance;
	private string activeLevel;			// Active level
	public bool isDead;
	public bool isObjectiveComplete;
	private bool hasWin;
	private bool hasStarted = false;

	// ---------------------------------------------------------------------------------------------------
	// gamestate()
	// --------------------------------------------------------------------------------------------------- 
	// Creates an instance of gamestate as a gameobject if an instance does not exist
	// ---------------------------------------------------------------------------------------------------
	public static gamestate Instance
	{
		get
		{
			if(instance == null)
			{
				instance = new GameObject("gamestate").AddComponent<gamestate>();
			}

			return instance;
		}
	}	

	// Sets the instance to null when the application quits
	public void OnApplicationQuit()
	{
		instance = null;
	}
	// ---------------------------------------------------------------------------------------------------


	// ---------------------------------------------------------------------------------------------------
	// startState()
	// --------------------------------------------------------------------------------------------------- 
	// Creates a new game state
	// ---------------------------------------------------------------------------------------------------
	public void startState()
	{
		print ("Creating a new game state");

		// Set default properties:
		activeLevel = "Level 1";
		isDead = false;
		isObjectiveComplete = false;
		hasWin = false;


		// Load level 1
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
	}

	// ---------------------------------------------------------------------------------------------------
	// getLevel()
	// --------------------------------------------------------------------------------------------------- 
	// Returns the currently active level
	// ---------------------------------------------------------------------------------------------------
	public string getLevel()
	{
		return activeLevel;
	}


	// ---------------------------------------------------------------------------------------------------
	// setLevel()
	// --------------------------------------------------------------------------------------------------- 
	// Sets the currently active level to a new value
	// ---------------------------------------------------------------------------------------------------
	public void setLevel(string newLevel)
	{
		// Set activeLevel to newLevel
		activeLevel = newLevel;
	}

	// ---------------------------------------------------------------------------------------------------
	// getName()
	// --------------------------------------------------------------------------------------------------- 
	// Returns the characters name
	// ---------------------------------------------------------------------------------------------------
	public string getName()
	{
		return name;
	}
		

	// ---------------------------------------------------------------------------------------------------
	// getDeathState()
	// --------------------------------------------------------------------------------------------------- 
	// Returns the death state
	// ---------------------------------------------------------------------------------------------------
	public bool getDeathState()
	{
		return isDead;
	}


	// ---------------------------------------------------------------------------------------------------
	// setDeathState()
	// --------------------------------------------------------------------------------------------------- 
	// Set character death state
	// ---------------------------------------------------------------------------------------------------
	public void setDeathState(bool newState)
	{
		isDead = newState;
	}


	// ---------------------------------------------------------------------------------------------------
	// getObjectiveComplete()
	// --------------------------------------------------------------------------------------------------- 
	// Returns the objectiveComplete
	// ---------------------------------------------------------------------------------------------------
	public bool getObjectiveComplete()
	{
		return isObjectiveComplete;
	}

	// ---------------------------------------------------------------------------------------------------
	// setObjectiveComplete()
	// --------------------------------------------------------------------------------------------------- 
	// Set the objectiveComplete
	// ---------------------------------------------------------------------------------------------------
	public void setObjectiveComplete(bool newState)
	{
		isObjectiveComplete = newState;
	}

	// ---------------------------------------------------------------------------------------------------
	// getWinState()
	// --------------------------------------------------------------------------------------------------- 
	// Returns the getWinState
	// ---------------------------------------------------------------------------------------------------
	public bool getWinState()
	{
		return hasWin;
	}

	// ---------------------------------------------------------------------------------------------------
	// setWinState()
	// --------------------------------------------------------------------------------------------------- 
	// Set the setWinState
	// ---------------------------------------------------------------------------------------------------
	public void setWinState(bool newState)
	{
		hasWin	 = newState;
	}

	public bool getStartState()
	{
		return hasStarted;
	}

	// ---------------------------------------------------------------------------------------------------
	// setWinState()
	// --------------------------------------------------------------------------------------------------- 
	// Set the setWinState
	// ---------------------------------------------------------------------------------------------------
	public void setStartState(bool newState)
	{
		hasStarted	 = newState;
	}
}