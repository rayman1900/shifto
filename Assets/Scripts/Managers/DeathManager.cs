﻿using UnityEngine;
using System.Collections;

public class DeathManager : MonoBehaviour {

	public bool isDead = false;

	// Use this for initialization
	void Start () {
		GameLib.EventManager.RegisterListener("Death", this.OnDeath);
	}


	private void OnDeath(object sender, object args){
		if (!isDead) {

			//Put death relative thing here




			isDead = true;
		}
	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy(){
		GameLib.EventManager.RemoveListener("Death", this.OnDeath);
	}
}
