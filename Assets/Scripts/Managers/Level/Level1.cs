﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Level1 : MonoBehaviour {

	public GameObject tutorial_UI;
	public GameObject objective_UI;
	public GameObject gameOver_UI;
	public GameObject nextForm_UI;

	public AudioSource gameOverSound;

	private AudioSource music;

	private bool isDead = false;

	private bool hasStartedGame = false;

	// Use this for initialization
	void Start () {

		//Set that the game hasn't started yet
		//gamestate.Instance.setStartState(false);

		tutorial_UI.SetActive (true);
		objective_UI.SetActive (false);
		gameOver_UI.SetActive (false);
		nextForm_UI.SetActive (false);

		music = Camera.main.GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.UpArrow) && !gamestate.Instance.getDeathState() && !hasStartedGame) {
			StartGame ();
			hasStartedGame = true;
		}

		if (gamestate.Instance.getDeathState()) {
			gameOver_UI.SetActive (true);


			if (!gameOverSound.isPlaying && !isDead) {
				gameOverSound.Play ();
			}


			FadeOutMusic ();

			isDead = true;
		}

		if (Input.GetKeyDown(KeyCode.UpArrow) && gamestate.Instance.getDeathState()) {
			gamestate.Instance.startState();
		}
	}


	private void StartGame(){
		gamestate.Instance.setStartState(true);
		nextForm_UI.SetActive (true);
		tutorial_UI.SetActive (false);
		StartCoroutine ("ShowUpObjective");
	}

	IEnumerator ShowUpObjective(){
		objective_UI.SetActive (true);
		yield return new WaitForSeconds (2.0f);
		objective_UI.SetActive (false);
	}

	private void FadeOutMusic(){
		music.volume -= 1 * Time.deltaTime;
	}
}
