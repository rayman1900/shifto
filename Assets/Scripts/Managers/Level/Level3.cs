﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Level3 : MonoBehaviour {

	public GameObject objective_UI;
	public GameObject GameOver_UI;

	public AudioSource gameOverSound;

	private AudioSource music;

	private bool isDead = false;

	// Use this for initialization
	void Start () {

		//Set that the game hasn't started yet

		objective_UI.SetActive (false);
		GameOver_UI.SetActive (false);

		music = Camera.main.GetComponent<AudioSource> ();

		StartGame ();
	}
	
	// Update is called once per frame
	void Update () {
		

		if (gamestate.Instance.getDeathState()) {
			GameOver_UI.SetActive (true);


			if (!gameOverSound.isPlaying && !isDead) {
				gameOverSound.Play ();
			}


			FadeOutMusic ();

			isDead = true;
		}

		if (Input.GetKeyDown(KeyCode.UpArrow) && gamestate.Instance.getDeathState()) {
			gamestate.Instance.startState();
		}
	}


	private void StartGame(){
		gamestate.Instance.setStartState(true);
		StartCoroutine ("ShowUpObjective");
	}

	IEnumerator ShowUpObjective(){
		objective_UI.SetActive (true);
		yield return new WaitForSeconds (2.0f);
		objective_UI.SetActive (false);
	}

	private void FadeOutMusic(){
		music.volume -= 1 * Time.deltaTime;
	}
}
