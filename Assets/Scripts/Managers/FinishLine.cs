﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class FinishLine : MonoBehaviour {

	public int currentLevelID;

	private GameObject player;
	public AudioSource finishSound;
	private AudioSource music;

	private bool hasEndedLevel = false;

	void Start(){
		player = GameObject.FindGameObjectWithTag ("Player");
		music = Camera.main.GetComponent<AudioSource> ();
	}

	void Update(){
		if (hasEndedLevel) {
			FadeOutMusic ();	
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			StartCoroutine ("WinningTriggering");
		}
	}


	IEnumerator WinningTriggering(){
		gamestate.Instance.setWinState (true);

		player.GetComponent<BouncingScript> ().stopMovement = true;

		if (!finishSound.isPlaying) {
			finishSound.Play ();
		}
		hasEndedLevel = true;

		yield return new WaitForSeconds (1f);

		if (currentLevelID == 3) {
			SceneManager.LoadScene (0);
		} else {
			SceneManager.LoadScene (currentLevelID + 1);	
		}

	}


	private void FadeOutMusic(){
		music.volume -= 1 * Time.deltaTime;
	}
}
