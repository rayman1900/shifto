﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ObjectiveManager : MonoBehaviour {

	public int currentLevelObjective;

	public int starCollected;

	public GameObject starUI;

	public GameObject ObjectiveCompleteUI;

	public GameObject redBorn;

	// Use this for initialization
	void Start () {
		GameLib.EventManager.RegisterListener("StarCollect", this.OnStarCollect);
		starCollected = 0;
		gamestate.Instance.setObjectiveComplete (false);
		ObjectiveCompleteUI.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (starCollected == currentLevelObjective && !gamestate.Instance.getObjectiveComplete()) {
			//print ("All star Collected");
			StartCoroutine("ShowUpObjectiveCompleteMessage");
			Destroy (redBorn);
			gamestate.Instance.setObjectiveComplete (true);
		}
	}


	private void OnStarCollect(object sender, object args){
		if (starCollected != currentLevelObjective) {
			starCollected++;
			starUI.GetComponent<Text> ().text = starCollected.ToString();
		}
	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy(){
		GameLib.EventManager.RemoveListener("StarCollect", this.OnStarCollect);
	}

	IEnumerator ShowUpObjectiveCompleteMessage(){
		ObjectiveCompleteUI.SetActive (true);
		yield return new WaitForSeconds (2.0f);
		ObjectiveCompleteUI.SetActive (false);
	}
}
