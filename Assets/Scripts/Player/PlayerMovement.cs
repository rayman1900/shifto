﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public float speed = 1.0f;
	public Vector2 velocity = new Vector2 (0, 300);
	public Vector2 maxVelocity = new Vector2(0, 1);

	private Rigidbody2D rb2D;
	private BoxCollider2D boxCollider;

	public AudioSource jump;

	// Use this for initialization
	void Start () {
		rb2D = GetComponent<Rigidbody2D> ();
		boxCollider = GetComponent<BoxCollider2D> ();
	}
	
	// Update is called once per frame
	void Update(){
		if (!gamestate.Instance.getDeathState () && gamestate.Instance.getStartState()) {
			PushForward ();		
		} else if (gamestate.Instance.getDeathState ()) {
			boxCollider.isTrigger = true;
		}
	}

	/// <summary>
	/// Will apply a force impulse in the upward direction when pressing the w or up key
	/// </summary>
	private void PushForward(){

		if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) {

			//Constrain the velocity on one direction
			if (rb2D.velocity.y > maxVelocity.y) {
				rb2D.velocity = maxVelocity.normalized;
			}

			if (rb2D.velocity.y <= 0) {
				rb2D.velocity = new Vector2 (0, 0);
			}

			rb2D.velocity = Vector2.zero;
			rb2D.AddForce(velocity);

			jump.Play ();

		}else if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.UpArrow)) {
			rb2D.velocity = Vector2.zero;
			rb2D.AddForce(velocity);
		}


	}
}
