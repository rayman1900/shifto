﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class LaneChanging : MonoBehaviour {

	public bool isUsingFiveLanes = false;

	public Transform laneMiddle;
	public Transform laneRight;
	public Transform laneLeft;
	public Transform laneFarLeft;
	public Transform laneFarRight;

	private int lanePosition;
	private int lastLanePosition;

	public AudioSource jumpSwitch;

	void Awake(){
		DOTween.Init();
	}

	// Use this for initialization
	void Start () {
		lanePosition = 0;
		transform.position = new Vector3 (laneMiddle.transform.position.x, transform.position.y, transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
		if (!gamestate.Instance.getDeathState() && gamestate.Instance.getStartState()) {
			ChangeLane ();	
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "VerticalRedBorn") {
			if (lastLanePosition < 0) {
				MoveToPosition ();
				lanePosition--;
			}else if (lastLanePosition > 0) {
				MoveToPosition ();
				lanePosition++;
			}
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if (other.tag == "VerticalRedBorn") {
			if (lastLanePosition < 0) {
				lanePosition = lastLanePosition;
			}else if (lastLanePosition > 0) {
				lanePosition = lastLanePosition;
			}
		}
	}

	private void MoveToPosition(){
		switch (lanePosition) {

		case 0:
			transform.DOLocalMoveX (laneMiddle.position.x, 0.2f, false);
			break;
		case 1:
			transform.DOLocalMoveX (laneRight.position.x, 0.2f, false);
			break;
		case -1:
			transform.DOLocalMoveX (laneLeft.position.x, 0.2f, false);
			break;
		case -2:
			if (isUsingFiveLanes) {
				transform.DOLocalMoveX (laneFarLeft.position.x, 0.2f, false);	
			}
			break;
		case 2:
			if (isUsingFiveLanes) {
				transform.DOLocalMoveX (laneFarRight.position.x, 0.2f, false);
			}
			break;
		default:
			break;
		}
	}

	private void ChangeLane(){

		if (isUsingFiveLanes) {

			if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)) {
				if (lanePosition - 1 >= -2) {
					lastLanePosition = lanePosition;
					lanePosition--;
					MoveToPosition ();
					jumpSwitch.Play ();
				}
			}

			if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) {
				if (lanePosition + 1 <= 2) {
					lastLanePosition = lanePosition;
					lanePosition++;
					MoveToPosition ();
					jumpSwitch.Play ();
				}
			}

		} else {
			if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)) {
				if (lanePosition - 1 >= -1) {
					lastLanePosition = lanePosition;
					lanePosition--;
					MoveToPosition ();
					jumpSwitch.Play ();
				}
			}

			if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) {
				if (lanePosition + 1 <= 1) {
					lastLanePosition = lanePosition;
					lanePosition++;
					MoveToPosition ();
					jumpSwitch.Play ();
				}
			}
		}
	}
}
