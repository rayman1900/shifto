﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShapeShift : MonoBehaviour {

	public Sprite[] forms;

	private List<Sprite> _forms = new List<Sprite>();
	private Sprite initialForm;
	private Sprite currentForm;
	private SpriteRenderer spr_rend;

	private int count = 0;

	// Use this for initialization
	void Start () {
		GameLib.EventManager.RegisterListener("NewFormCollect", this.NewFormCollect);



		//Add forms sprite in the list
		foreach (var item in forms) {
			_forms.Add (item);
		}

		//Start with the default form
		initialForm = _forms[0];

		spr_rend = GetComponent<SpriteRenderer> ();
		spr_rend.sprite = initialForm;
	}
	
	// Update is called once per frame
	void Update () {
		if (!gamestate.Instance.getDeathState() && gamestate.Instance.getStartState()) {
			Shift ();	
		}
	}

	/// <summary>
	/// Shape shift the player by pressing the up or w arrow
	/// </summary>
	private void Shift(){
		
		if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift)) {

			count++;

			if (count > _forms.Count - 1) {
				count = 0;
			}

			spr_rend.sprite = _forms[count];
		}
	}

	private void NewFormCollect(object sender, object args){
		//Collect sprite from sender
		SomeArgs theArgs = args as SomeArgs;

		_forms.Add (theArgs.SomeParameterThatMightBeAClass as Sprite);

	}


	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy(){
		GameLib.EventManager.RemoveListener("NewFormCollect", this.NewFormCollect);
	}
}
