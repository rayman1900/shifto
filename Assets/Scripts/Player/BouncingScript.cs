﻿using UnityEngine;
using System.Collections;

public class BouncingScript : MonoBehaviour {

	[SerializeField]
	private float maxVerticalVelocity = 7;
	[SerializeField]
	private float minVerticalVelocity = -7f;
	[SerializeField]
	private float climbSpeed = .05f;
	[SerializeField]
	private float climbSpeedWhenReversing = .1f;
	[SerializeField]
	private float jumpAmount = 5f;
	[SerializeField]
	private float fallSpeed = .2f;

	private float verticalVelocity = 0f;
	private bool moving;

	public bool stopMovement = false;

	public AudioSource jump;

	// Use this for initialization
	void Start () {

		moving = false;

	}

	// Update is called once per frame
	void Update () {
		if (!gamestate.Instance.getDeathState () && gamestate.Instance.getStartState () && !stopMovement) {
			CheckPlayerMov ();
		} else if (gamestate.Instance.getDeathState ()) {

			GetComponent<BoxCollider2D> ().enabled = false;

			verticalVelocity -= fallSpeed;

			if (verticalVelocity < minVerticalVelocity)
			{
				verticalVelocity = minVerticalVelocity;
			}

			transform.Translate(0, verticalVelocity*Time.deltaTime, 0);

		}else if (stopMovement) {
			//Do nothing
		}
	}


	private void CheckPlayerMov(){
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			moving = true;

			verticalVelocity = jumpAmount;

			jump.Play ();
		}
		else if (Input.GetKey(KeyCode.UpArrow))
		{
			if (verticalVelocity >= 0)
			{
				verticalVelocity += climbSpeed;
			}
			else if (verticalVelocity < 0)
			{
				verticalVelocity += climbSpeedWhenReversing;
			}
				
			if (verticalVelocity > maxVerticalVelocity)
				verticalVelocity = maxVerticalVelocity;
		}
		else
		{
			verticalVelocity -= fallSpeed;

			if (verticalVelocity < minVerticalVelocity)
			{
				verticalVelocity = minVerticalVelocity;
			}
		}

		if (moving)
			transform.Translate(0, verticalVelocity*Time.deltaTime, 0);
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.tag == "Ground" && !gamestate.Instance.getDeathState ()) {
			moving = false;
		}else if (other.gameObject.tag == "Ground" && gamestate.Instance.getDeathState ()) {
			other.collider.enabled = false;
		}
	}
}
