﻿using UnityEngine;
using System.Collections;

public class CollisionChecker : MonoBehaviour {

	public string formCheck;

	private string analysedForm = "";
	private BoxCollider2D boxCollider;

	// Use this for initializationdsd
	void Start () {
		boxCollider = GetComponent<BoxCollider2D> ();
	}

	void OnTriggerStay2D(Collider2D other){
		if (other.tag == "Player") {
			analysedForm = other.GetComponent<SpriteRenderer> ().sprite.name;

			//Check if going over the bound of the collider
			if (other.bounds.center.y >= boxCollider.bounds.min.y) {
				if (analysedForm == formCheck) { //Is the form OK?
					//print ("good");

					//Layer change
					other.GetComponent<SpriteRenderer> ().sortingOrder = -2;

				} else { //Else, kill the player
					if (!gamestate.Instance.getDeathState()){
						gamestate.Instance.setDeathState (true);
					}
				}
			}else if (other.bounds.center.y <= (boxCollider.bounds.size.y/3)) { //Check if going below the center of the collider
				//Layer change
				other.GetComponent<SpriteRenderer> ().sortingOrder = 2;
			} 
		}
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.tag == "Player") {
			if (!gamestate.Instance.getDeathState()){
				gamestate.Instance.setDeathState (true);
			}
		}
	}


	void OnTriggerExit2D(Collider2D other){
		if (other.tag == "Player") {
			//If quiting collider, return to initial sorting order
			other.GetComponent<SpriteRenderer> ().sortingOrder = 1;
		}
	}
}
