﻿using UnityEngine;
using System.Collections;

public class RedBorn : MonoBehaviour {

	public GameObject objective_UI;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.tag == "Player") {

			StartCoroutine ("ShowUpObjective");
		}
	}

	IEnumerator ShowUpObjective(){
		objective_UI.SetActive (true);
		yield return new WaitForSeconds (1.5f);
		objective_UI.SetActive (false);
	}
}
