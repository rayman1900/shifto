﻿using UnityEngine;
using System.Collections;

public class OrderLayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.GetComponent<MeshRenderer>().sortingLayerID = this.transform.parent.GetComponent<SpriteRenderer>().sortingLayerID;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
